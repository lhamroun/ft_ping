#include "ft_ping.h"

extern t_ping	*g_ping;

void			ft_usleep(unsigned int duration)
{
	unsigned long	start;

	start = get_time_now();
	while (get_time_now() - start < (unsigned long)duration)
	{
	}
}

unsigned long	get_time_now(void)
{
	struct timeval	timestamp;

	gettimeofday(&timestamp, NULL);
	return ((unsigned long)timestamp.tv_sec * 1000000 + (unsigned long)timestamp.tv_usec);
}

unsigned long	timeval_to_ul(struct timeval tv)
{
	return ((unsigned long)tv.tv_sec * 1000000 + (unsigned long)tv.tv_usec);
}

double	update_rtt_curr(struct timeval tv)
{
	unsigned long	t1;
	unsigned long	t2;

	(void)tv;
	t1 = timeval_to_ul(g_ping->rtt.tv);
	t2 = get_time_now();
	if (t1 > t2)
	{
		return (double)((ULONG_MAX - t2 + t1) / 1000);
	}
	return (double)(t2 - t1) / 1000;
}

double	update_rtt_min(void)
{
	if (g_ping->rtt.curr < g_ping->rtt.min)
		return g_ping->rtt.curr;
	return g_ping->rtt.min;
}

double	update_rtt_max(void)
{
	if (g_ping->rtt.curr > g_ping->rtt.max)
		return g_ping->rtt.curr;
	return g_ping->rtt.max;
}

double	update_rtt_avg(void)
{
	return (g_ping->rtt.average + g_ping->rtt.curr) / 2;
}

double	update_rtt_stddev(void)
{
	return (g_ping->rtt.curr + g_ping->rtt.average) / 2;
}

t_time	update_rtt_stat(void)
{
	struct timeval	tmp;

	gettimeofday(&tmp, NULL);
	g_ping->rtt.curr = update_rtt_curr(tmp);
	g_ping->rtt.min = update_rtt_min();
	g_ping->rtt.max = update_rtt_max();
	g_ping->rtt.average = update_rtt_avg();
	g_ping->rtt.stddev = update_rtt_stddev();
	g_ping->rtt.tv = tmp;
	return g_ping->rtt;
}
