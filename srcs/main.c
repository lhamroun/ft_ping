#include "ft_ping.h"

/*
	__attribute__((destructor)) void loop(void){while(1);}
//*/

void	usage(int ret)
{
	printf("usage: ./ft_ping [-v] [-h] [-q] [-n [int]] [-t [int]] [-c [int]] <ipv4>\n	-v: verbose\n	-h: help\n	-q: quiet\n	-n: ttl max (0 < x < 256)\n	-t: timeout (0 < y < INT_MAX)\n	-c: remaining packets (0 < z < INT_MAX)\n");
	exit(ret);
}

t_bool	is_valid_number(char *str)
{
	int		i = 0;

	while (str[i])
	{
		if (!ft_isdigit(str[i]))
			return FALSE;
		i++;
	}
	return TRUE;
}

t_bool	is_valid_ip(char *ip)
{
	int		num = 0;
	int		dots = 0;
	size_t	cpt = 0;
	char	*ptr;
	char	*tmp;

	if (ip == NULL)
		return FALSE;
	ptr = ft_strdup(ip);
	tmp = ptr;
	if (!ptr)
		return FALSE;
	ptr = strtok(ptr, ".");
	for (size_t i = 0; i < ft_strlen(ip); i++)
	{
		if (ft_isdigit(ip[i]))
			cpt++;
	}
	if (cpt == ft_strlen(ip))
	{
		ft_strdel(&tmp);
		return FALSE;
	}
	if (ft_isalpha(ip[0]))
	{
		ft_strdel(&tmp);
		return TRUE;
	}
	while (ptr)
	{
		if (ft_isalpha(*ptr))
			break ;
		else if (!is_valid_number(ptr))
		{
			ft_strdel(&tmp);
			return FALSE;
		}
		num = ft_atoi(ptr);
		if (num >= 0 && num <= 255)
		{
			ptr = strtok(NULL, ".");
			if (ptr != NULL)
				dots++;
		}
		else
		{
			printf("ping: %s: Name or service not known\n", ip);
			ft_strdel(&tmp);
			return FALSE;
		}
	}
	ft_strdel(&tmp);
	return TRUE;
	return dots != 3 ? FALSE : TRUE;
}

t_bool	check_ip(char *addr)
{
	if (!ft_isalpha(addr[0]) && !ft_isdigit(addr[0]))
		return FALSE;
	if (!is_valid_ip(addr))
		return FALSE;
	return TRUE;
}

t_args	parsing(char **av)
{
	t_args	args;

	ft_memset(&args, 0, sizeof(t_args));
	args.rtt = -1;
	args.ttl_max = -1;
	args.count = -1;
	if (!array_len(av) || array_len(av) > 10)
		usage(1);
	for (int i = 0; i < array_len(av); i++)
	{
		if (check_ip(av[i]) && args.addr == NULL)
			args.addr = av[i];
		else if (!ft_strcmp(av[i], "-v") && args.verbose == 0)
			args.verbose = 1;
		else if (!ft_strcmp(av[i], "-h") && args.help == 0)
			args.help = 1;
		else if (!ft_strcmp(av[i], "-q") && args.quiet == 0)
			args.quiet = 1;
		else if (!ft_strcmp(av[i], "-t") && args.rtt == -1 && av[i + 1])
		{
			if (ft_atoi(av[i + 1]) > 0 && ft_atoi(av[i + 1]) < INT_MAX)
				args.rtt = ft_atoi(av[i + 1]);
			else
				usage(1);
		}
		else if (!ft_strcmp(av[i], "-c") && args.count == -1 && av[i + 1])
		{
			if (ft_atoi(av[i + 1]) > 0)
				args.count = ft_atoi(av[i + 1]);
			else
				usage(1);
		}
		else if (!ft_strcmp(av[i], "-n") && args.ttl_max == -1 && av[i + 1])
		{
			if (ft_atoi(av[i + 1]) > 0 && ft_atoi(av[i + 1]) < 256)
				args.ttl_max = ft_atoi(av[i + 1]);
			else
				usage(1);
		}
		else if (ft_atoi(av[i]))
			continue ;
		else
			usage(1);
	}
	if (args.addr == NULL)
	{
		ft_memset(&args, 0, sizeof(t_args));
		return args;
	}
	return args;
}

int		main(int ac, char **av)
{
	t_args	args;

	(void)ac;
	if (getuid())
	{
		printf("You need to be root to run ./ft_ping\n");
		return 1;
	}
	args = parsing(av + 1);
	if (args.help != 0)
		usage(0);
	else if (!args.addr)
	{
		usage(1);
		return 1;
	}
	return ft_ping(args);
}

