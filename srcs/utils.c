#include "ft_ping.h"

extern t_ping	*g_ping;

void	clean_memory(void)
{
	freeaddrinfo(g_ping->serverInfo);
	ft_strdel(&g_ping->host);
	close(g_ping->fd_socket);
	free(g_ping);
}
