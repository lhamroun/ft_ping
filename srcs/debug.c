#include <ft_ping.h>

void	print_ping(t_ping *ping)
{
	printf("------------------------------------------\n");
	printf("socket fd   : %d\n", ping->fd_socket);
	printf("ttl         : %u\n", ping->ttl);
	printf("packetSize  : %u\n", ping->packetSize);
	printf("nb packet   : %u\n", ping->nbPkt);
	printf("lost packet : %u\n", ping->pktLost);
	printf("host        : %s (%s / %x)\n", ping->args.addr, ping->host, ping->host_ul);
	printf("startTime   : %lu\n", timeval_to_ul(ping->startTime));
	printf("------------------------------------------\n");
}

void	print_rtt(t_time t)
{
	printf("______________________________________________\n");
	printf("curr   : %lf\n", t.curr);
	printf("min    : %lf\n", t.min);
	printf("max    : %lf\n", t.max);
	printf("avg    : %lf\n", t.average);
	printf("stddev : %lf\n", t.stddev);
	printf("______________________________________________\n");
}

void	print_msghdr(struct msghdr msg)
{
	printf("=============================================\n");
	printf("msg_flags   %d\n", msg.msg_flags);
	if (msg.msg_namelen)
	{
		printf("msg_name    %s\n", (char*)msg.msg_name);
		printf("msg_namelen %lu\n", (size_t)msg.msg_namelen);
	}
	else
		printf("msg_namelen %lu\n", (size_t)msg.msg_namelen);
	if (msg.msg_iov)
	{
		for (int i = 0; i < (int)msg.msg_iovlen; i++)
		{
			printf("	index %d\n", i);
			printf("	iov_base %s\n", (char*)msg.msg_iov[i].iov_base);
			printf("	iov_len %zu\n", msg.msg_iov[i].iov_len);
			printf("===   ===   ===   ===\n");
		}
	}
	else
		printf("msg_iov     %p\n", msg.msg_iov);
	printf("msg_iovlen  %lu\n", (size_t)msg.msg_iovlen);
	if (msg.msg_control)
	{
		printf("==================\n");
		struct cmsghdr	*tmp = (struct cmsghdr *)msg.msg_control;
		printf("cmsg_len    %lu\n", (size_t)tmp->cmsg_len);
		printf("cmsg_level  %d\n", tmp->cmsg_level);
		printf("cmsg_type   %d\n", tmp->cmsg_type);
		printf("cmsg_data   %s\n", (char *)&tmp->cmsg_type + sizeof(tmp->cmsg_type));
	}
	else
		printf("msg_control %p\n", msg.msg_control);
	printf("msg_controllen %lu\n", (size_t)msg.msg_controllen);
	printf("=============================================\n");
}

void	print_pkt(t_pkt pkt)
{
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	printf("icmp type       : %hhu\n", pkt.icmp.type);
	printf("icmp code       : %hhu\n", pkt.icmp.code);
	printf("icmp checksum   : %hx\n", pkt.icmp.checksum);
#ifdef __MAC__
	printf("icmp un id      : %hu\n", pkt.icmp.id);
	printf("icmp un sequence: %hu\n", pkt.icmp.sequence);
#else
	printf("icmp un id      : %hu\n", pkt.icmp.un.echo.id);
	printf("icmp un sequence: %hu\n", pkt.icmp.un.echo.sequence);
#endif
	printf("pkt msg         : %s\n", pkt.msg);
	printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
}

void	print_icmp(struct icmphdr icmp)
{
	printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
	printf("icmp type       : %hhu\n", icmp.type);
	printf("icmp code       : %hhu\n", icmp.code);
	printf("icmp checksum   : %hx\n", icmp.checksum);
#ifdef __MAC__
	printf("icmp un id      : %hu\n", icmp.id);
	printf("icmp un sequence: %hu\n", icmp.sequence);
#else
	printf("icmp un id      : %hu\n", icmp.un.echo.id);
	printf("icmp un sequence: %hu\n", icmp.un.echo.sequence);
#endif
	printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
}

void	print_ip(struct iphdr ip)
{
	printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
	printf("ip version    : %d\n", (int)ip.version);
	printf("ip ihl        : %d\n", (int)ip.ihl);
	printf("ip id         : %hu\n", ip.id);
	printf("ip ttl        : %hhu\n", ip.ttl);
	printf("ip protocol   : %hhu\n", ip.protocol);
	printf("ip check      : %hx\n", ip.check);
	printf("ip saddr      : %x\n", ip.saddr);
	printf("ip daddr      : %x\n", ip.daddr);
	printf("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~\n");
}

void	print_addrinfo(struct addrinfo server)
{
	printf("##################################################\n");
	printf("ai_flags    : %d\n", server.ai_flags);
	printf("ai_family   : %d\n", server.ai_family);
	printf("ai_socktype ; %d\n", server.ai_socktype);
	printf("ai_protocol : %d\n", server.ai_protocol);
	printf("ai_addrlen  : %u\n", server.ai_addrlen);
	printf("ai_canonname: %s\n", server.ai_canonname);
	printf("ai_next     : %p\n", server.ai_next);
	if (server.ai_addr)
	{
		printf("ai_addr->sa_family: %hu\n", ((struct sockaddr_in *)server.ai_addr)->sin_family);
		printf("ai_addr->sa_port  : %hu\n", ((struct sockaddr_in *)server.ai_addr)->sin_port);
		printf("ai_addr->sa_addr  : %x\n", ((struct sockaddr_in *)server.ai_addr)->sin_addr.s_addr);
	}
	printf("##################################################\n");
}
