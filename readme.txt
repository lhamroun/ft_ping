inet_addr("10.19.240.121") --> convertit l'addresse en parametre en int

inet_ntoa(2045776650) --> covertit l'int en parametre en "10.19.240.121"

inet_ntop() --> convertit l'addresse int en parametre en string

inet_pton() --> convertit l'addresse string (pas une addresse DNS mais addresse IP au format string) en int --> specifier AF_UNSPEC pour les addresses invalides

getaddrinfo() --> a partir d'une addresse DNS string, et d'une struct addrinfo contenant les caractéristique de cette addresse, remplit une struct addrinfo avec toutes les datas liées à l'ip

struct addrinfo --> hold host informations

struct sockaddr --> struct générique, equivalente a sockaddr_in, specifies a transport address and port for the AF_INET address family



ref:
socket: https://fr.manpages.org/socket/7
raw socket: http://www.man-linux-magique.net/man7/raw.html
IPPROTO_ICMP option: http://www.man-linux-magique.net/man7/IPPROTO_ICMP.html

leaks on getaddrinfo : https://bugs.launchpad.net/ubuntu/+source/glibc/+bug/1740569

###############################################################################
TODO  #########################################################################
###############################################################################

