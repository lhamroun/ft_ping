NAME = ft_ping

FLAG = -Wall -Wextra -Werror -D_GNU_SOURCE

OS_NAME := $(shell uname)
ifeq ($(OS_NAME),Darwin)
	FLAG += -D __MAC__
endif

HEADER = includes/ft_ping.h includes/icmp.h
INCLUDES = -I includes/

SRCS_PATH = srcs/
SRCS_NAME = main.c ft_count_words.c ft_ping.c time.c packet.c check_packet.c debug.c utils.c
SRCS_NAME += ft_memalloc.c ft_memset.c ft_atoi.c ft_strcmp.c ft_strdel.c ft_isdigit.c ptr2Ddel.c ft_isalpha.c ft_strlen.c array_len.c ft_strchr.c ft_strdup.c ft_memchr.c
UTILS = $(addprefix $(UTILS_PATH), $(UTILS_NAME))
SRCS = $(addprefix $(SRCS_PATH),$(SRCS_NAME))

OBJS_PATH = .objs/
OBJS_NAME = $(SRCS_NAME:.c=.o)
OBJS = $(addprefix $(OBJS_PATH),$(OBJS_NAME))

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(FLAG) $(INCLUDES) -o $(NAME) $(OBJS)

$(OBJS_PATH)%.o:$(SRCS_PATH)%.c $(HEADER)
	mkdir -p $(OBJS_PATH)
	$(CC) $(FLAG) $(INCLUDES) -o $@ -c $<

clean:
	$(RM) -rf $(OBJS_PATH)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
